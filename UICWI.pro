######################################################################
# Automatically generated by qmake (3.0) Wed Dec 12 22:32:44 2018
######################################################################

TEMPLATE = app
TARGET = UICWI
INCLUDEPATH += .
QT += widgets
LIBS += -lgit2

# Input
HEADERS += checkouttab.h \
           commit.h \
           foldertab.hpp \
           gitpp7.h \
           keywordsearch.hpp \
           tabdialog.h \
           configtab.h \
           varcontainer.h
SOURCES += checkouttab.cpp \
           commit.cpp \
           foldertab.cpp \
           keywordsearch.cpp \
           main.cpp \
           tabdialog.cpp \
           configtab.cpp \
           varcontainer.cpp
