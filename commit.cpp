#include <QtWidgets>

#include "gitpp7.h"
#include "commit.h"

CommitHistoryTab::CommitHistoryTab(QWidget *parent)
    : QWidget(parent)
{
  branchListBox = new QListWidget;
  QVBoxLayout *area = new QVBoxLayout;
  QTextEdit *text = new QTextEdit();
  try{
    GITPP::REPO r;


    text->setText("LIST OF COMMITS:\n");
    std::string message;
    for(auto i : r.commits())
    {
      message = i.message();
      QString qmessage = QString::fromStdString(message);
      text->append(qmessage);
    }
    text->setReadOnly(true);

    area->addWidget(text);
    setLayout(area);
  } catch(...)
  {
    QErrorMessage *noRepoErrorBox = new QErrorMessage();

    noRepoErrorBox->showMessage("No Repository Selected.");
  }
}
