#include <QtWidgets>
#include <string>
#include <iostream>


#include "gitpp7.h"
#include "keywordsearch.hpp"

KeywordSearch::KeywordSearch(QWidget *parent)
    : QWidget(parent)
{
  //create linedit
  searchInputBox = new QLineEdit();


  //create button
  searchButton = new QPushButton("Search", this);
  searchButton->setFixedSize(QSize(100, 30));
  connect(searchButton, SIGNAL (released()), this, SLOT (keywordSearch()));

  //create layout
  QVBoxLayout *layout = new QVBoxLayout;
  layout->addWidget(searchInputBox);
  layout->addWidget(searchButton);
  setLayout(layout);
}

void KeywordSearch::keywordSearch()
{
  try {
    GITPP::REPO r;

    //create results box
    searchResultBox = new QListWidget();

    for(auto i : r.commits())
    {
      std::string message = i.message();
      if (message.find(searchInputBox->text().toUtf8().constData()) != std::string::npos) {
        searchResultBox->insertItem(0, QString::fromStdString(message));
      }

    }

    QVBoxLayout *resultLayout = new QVBoxLayout;
    resultLayout->addWidget(searchResultBox);

    QWidget *wdg = new QWidget;
    wdg->setWindowTitle(tr("Git interface"));
    wdg->setLayout(resultLayout);
    wdg->show();
  }
  catch (...)
  {
    QErrorMessage *noRepoErrorBox = new QErrorMessage();

    noRepoErrorBox->showMessage("No Repository Selected.");
  }

}
