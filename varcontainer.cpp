#include "varcontainer.h"
#include "configtab.h"
#include <iostream>
#include <string>
#include <sstream>
#include <QDebug>

varContainer::varContainer(QWidget *parent, std::string fullVarName, std::string varValue, QWidget *topParent) : QWidget(parent)
{
   mainWindow = topParent;
   _fullVarName = fullVarName;
   _varValue = varValue;

   std::string subStr = fullVarName.substr(fullVarName.find(".")+1);
   QHBoxLayout *containerBox = new QHBoxLayout();
   std::string labelString = subStr + " = " + varValue;
   QString Qvar = QString::fromStdString(labelString);
   varLabel = new QLabel(Qvar);
   editButton = new QPushButton();
   QPixmap editPix("./icons/editIcon.png");
   QIcon buttonIcon(editPix.scaled(25,25));
   editButton->setIcon(buttonIcon);
   editButton->setMaximumSize(25,30);

   containerBox->addWidget(varLabel);
   containerBox->addWidget(editButton);
   containerBox->setContentsMargins(0,0,0,0);

   setLayout(containerBox);

   connect(editButton, SIGNAL(clicked()),
           this, SLOT(clickedSlot()));

   connect(this, SIGNAL(editClicked(varContainer*)),
   topParent, SLOT(setRightLayout(varContainer*)));
}

void varContainer::clickedSlot() {
   emit editClicked(this);
}
